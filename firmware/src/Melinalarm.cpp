#include <Arduino.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <TimeAlarms.h>
#include <Wire.h>
#include <SevSeg.h>

// #define SET_TIME
#ifdef SET_TIME
#include "SetTime.h"
#endif

#define COUNTDOWN_LENGTH 3000

#define INC_HR(h) (((h) + 1) % 24)
#define DEC_HR(h) (((h) + 23) % 24)
#define INC_MIN(m) (((m) + 1) % 60)
#define DEC_MIN(m) (((m) + 59) % 60)

typedef enum {
  stateShowTime,
  stateSetTimeHr,
  stateSetTimeMin,
  stateShowAlarm1,
  stateSetAlarm1Hr,
  stateSetAlarm1Min,
  stateShowAlarm2,
  stateSetAlarm2Hr,
  stateSetAlarm2Min,
} state_t;

typedef enum {
  noEvent,
  pendingEvent,
  leftBtnPress,
  rightBtnPress,
  bothBtnsPress,
  leftBtnReleaseShort,
  rightBtnReleaseShort,
  bothBtnsReleaseShort,
  leftBtnHold,
  rightBtnHold,
  bothBtnsHold,
  leftBtnReleaseLong,
  rightBtnReleaseLong,
  bothBtnsReleaseLong
} event_t;

void showHrMin(int8_t hr, int8_t min);
void showTime(void);
void alarm1Callback(void);
void alarm2Callback(void);
uint8_t senseLight(void);
event_t readButtons(void);
void buzzerOn(void);
void buzzerOff(void);

uint8_t digitPins[]   = {8,9,10,11}; // const
uint8_t segmentPins[] = {0,1,2,3,4,5,6,7}; // const
SevSeg sevseg(digitPins, segmentPins, 4, COMMON_CATHODE, DIGIT_RESISTORS, REVERSED_DECIMALS);

time_t t;
tmElements_t tmSet;
AlarmId alarm1Id;
AlarmId alarm2Id;
uint32_t btnPressTime;
uint32_t countdownTime;
int8_t alarm1Hr  =  8;
int8_t alarm1Min = 30;
int8_t alarm2Hr  =  9;
int8_t alarm2Min = 00;
bool leftBtn  = false;
bool rightBtn = false;
bool bothBtns = false;
bool halfSec  = false;
bool alarming = false;
bool alarm1On = false;
bool alarm2On = false;
bool time24hr = false;
bool blank[4] = {false, false, false, false};
bool decimals[4] = {alarm1On, alarm2On, true, false};
state_t state = stateShowTime;


void setup(void) {
  #ifdef SET_TIME
  setTime();
  #endif

  // Setup time and RTC (DS1307), sync every 10 minutes
  setSyncProvider(RTC.get);
  setSyncInterval(600); // 600 seconds = 10 minutes
  if (timeStatus() != timeSet && RTC.chipPresent()) {}

  // Setup seven-segment display
  sevseg.begin();

  // Setup photocell
  pinMode(A0, INPUT);

  // Setup buttons
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);

  t = now();
  countdownTime = millis();
  alarm1Id = Alarm.alarmRepeat(alarm1Hr, alarm1Min, 0, alarm1Callback);
  alarm2Id = Alarm.alarmRepeat(alarm2Hr, alarm2Min, 0, alarm2Callback);
}

void loop(void) {
  event_t ev = readButtons();

  // React to button events
  switch (ev) {
    case leftBtnReleaseShort:
      if (alarming) {
        alarming = false;
        buzzerOff();
      } else {
        switch (state) {
          case stateShowTime:
            if (!alarm1On) {
              alarm1On = true;
              decimals[0] = true;
              Alarm.enable(alarm1Id);
              state = stateShowAlarm1;
              halfSec = true;
              countdownTime = millis();
            } else {
              alarm1On = false;
              decimals[0] = false;
              Alarm.disable(alarm1Id);
            }
            break;
          case stateShowAlarm1:
            if (alarm1On) {
              alarm1On = false;
              decimals[0] = false;
              Alarm.disable(alarm1Id);
              state = stateShowTime;
              halfSec = false;
              countdownTime = millis();
            }
            break;
          case stateSetTimeHr:
            // Decrement time hour
            tmSet.Hour = DEC_HR(tmSet.Hour);
            countdownTime = millis();
            break;
          case stateSetTimeMin:
            // Decrement time minute
            tmSet.Minute = DEC_MIN(tmSet.Minute);
            countdownTime = millis();
            break;
          case stateSetAlarm1Hr:
            // Decrement alarm 1 hour
            alarm1Hr = DEC_HR(alarm1Hr);
            countdownTime = millis();
            break;
          case stateSetAlarm1Min:
            // Decrement alarm 1 minute
            alarm1Min = DEC_MIN(alarm1Min);
            countdownTime = millis();
            break;
          case stateSetAlarm2Hr:
            // Decrement alarm 2 hour
            alarm2Hr = DEC_HR(alarm2Hr);
            countdownTime = millis();
            break;
          case stateSetAlarm2Min:
            // Decrement alarm 2 minute
            alarm2Min = DEC_MIN(alarm2Min);
            countdownTime = millis();
            break;
          default:
            break;
        }
        showTime();
      }
      break;
    case rightBtnReleaseShort:
      if (alarming) {
        alarming = false;
        buzzerOff();
      } else {
        switch (state) {
          case stateShowTime:
            if (!alarm2On) {
              alarm2On = true;
              decimals[1] = true;
              Alarm.enable(alarm2Id);
              state = stateShowAlarm2;
              halfSec = true;
              countdownTime = millis();
            } else {
              alarm2On = false;
              decimals[1] = false;
              Alarm.disable(alarm2Id);
            }
            break;
          case stateShowAlarm2:
            if (alarm2On) {
              alarm2On = false;
              decimals[1] = false;
              Alarm.disable(alarm2Id);
              state = stateShowTime;
              halfSec = false;
              countdownTime = millis();
            }
            break;
          case stateSetTimeHr:
            // Increment time hour
            tmSet.Hour = INC_HR(tmSet.Hour);
            countdownTime = millis();
            break;
          case stateSetTimeMin:
            // Increment time minute
            tmSet.Minute = INC_MIN(tmSet.Minute);
            countdownTime = millis();
            break;
          case stateSetAlarm1Hr:
            // Increment alarm 1 hour
            alarm1Hr = INC_HR(alarm1Hr);
            countdownTime = millis();
            break;
          case stateSetAlarm1Min:
            // Increment alarm 1 minute
            alarm1Min = INC_MIN(alarm1Min);
            countdownTime = millis();
            break;
          case stateSetAlarm2Hr:
            // Increment alarm 2 hour
            alarm2Hr = INC_HR(alarm2Hr);
            countdownTime = millis();
            break;
          case stateSetAlarm2Min:
            // Increment alarm 2 minute
            alarm2Min = INC_MIN(alarm2Min);
            countdownTime = millis();
            break;
          default:
            break;
        }
        showTime();
      }
      break;
    case leftBtnHold:
      if (state == stateShowTime) {
        alarm1On = true;
        decimals[0] = true;
        Alarm.disable(alarm1Id);
        state = stateSetAlarm1Hr;
        countdownTime = millis();
        showTime();
      }
      break;
    case rightBtnHold:
      if (state == stateShowTime) {
        alarm2On = true;
        decimals[1] = true;
        Alarm.disable(alarm2Id);
        state = stateSetAlarm2Hr;
        countdownTime = millis();
        showTime();
      }
      break;
    case bothBtnsHold:
      if (state == stateShowTime) {
        breakTime(t, tmSet);
        state = stateSetTimeHr;
        countdownTime = millis();
        showTime();
      }
      break;
    default:
      break;
  }

  // Update the time every second
  if (state == stateShowTime && t != now()) {
    // Check the alarm scheduler every minute
    if (minute(t) != minute(now())) {
      Alarm.delay(0);
    }
    if (alarming) {
      buzzerOn();
    }

    t = now();
    halfSec = false;
    countdownTime = millis();
  }

  // Do stuff every half-second
  if (!halfSec && ((millis() - countdownTime) % 1000) < 500) {
    halfSec = true;
    sevseg.setBrightness(senseLight());
    showTime();
    decimals[2] = true;
  } else if (halfSec && ((millis() - countdownTime) % 1000) >= 500) {
    halfSec = false;
    sevseg.setBrightness(senseLight());
    showTime();

    switch (state) {
      case stateShowTime:
        decimals[2] = false;
        if (alarming) {
          buzzerOff();
        }
        break;
      case stateSetTimeHr:   // Falls through
      case stateSetAlarm1Hr: // Falls through
      case stateSetAlarm2Hr:
        // Turn off the first two digits
        blank[0] = true;
        blank[1] = true;
        blank[2] = false;
        blank[3] = false;
        sevseg.blank(blank);
        break;
      case stateSetTimeMin:   // Falls through
      case stateSetAlarm1Min: // Falls through
      case stateSetAlarm2Min:
        // Turn off the last two digits
        blank[0] = false;
        blank[1] = false;
        blank[2] = true;
        blank[3] = true;
        sevseg.blank(blank);
        break;
      default:
        break;
    }
  }

  // Finite-state machine
  if (millis() - countdownTime > COUNTDOWN_LENGTH) {
    halfSec = false;
    switch (state) {
      case stateShowAlarm1: // Falls through
      case stateShowAlarm2:
        state = stateShowTime;
        break;
      case stateSetTimeHr:
        state = stateSetTimeMin;
        halfSec = true;
        break;
      case stateSetAlarm1Hr:
        state = stateSetAlarm1Min;
        halfSec = true;
        break;
      case stateSetAlarm2Hr:
        state = stateSetAlarm2Min;
        halfSec = true;
        break;
      case stateSetTimeMin:
        tmSet.Second = COUNTDOWN_LENGTH / 1000;
        RTC.write(tmSet);
        setTime(makeTime(tmSet));
        state = stateShowTime;
        break;
      case stateSetAlarm1Min:
        Alarm.write(alarm1Id, AlarmHMS(alarm1Hr, alarm1Min, COUNTDOWN_LENGTH / 1000));
        Alarm.enable(alarm1Id);
        state = stateShowTime;
        break;
      case stateSetAlarm2Min:
        Alarm.write(alarm2Id, AlarmHMS(alarm2Hr, alarm2Min, COUNTDOWN_LENGTH / 1000));
        Alarm.enable(alarm2Id);
        state = stateShowTime;
        break;
      default:
        break;
    }
    countdownTime = millis();
  }

  // Update the decimals and refresh the display
  sevseg.setDecimals(decimals);
  sevseg.refresh();
}

void showHrMin(int8_t hr, int8_t min) {
  decimals[3] = !time24hr && hr >= 12;
  sevseg.setNumber((time24hr ? hr : (hr % 12 ? hr % 12 : 12)) * 100 + min, 0, false, time24hr);
}

void showTime(void) {
  switch (state) {
    case stateShowTime:
      showHrMin(hour(t), minute(t));
      break;
    case stateSetTimeHr: // Falls through
    case stateSetTimeMin:
      showHrMin(tmSet.Hour, tmSet.Minute);
      break;
    case stateShowAlarm1:  // Falls through
    case stateSetAlarm1Hr: // Falls through
    case stateSetAlarm1Min:
      showHrMin(alarm1Hr, alarm1Min);
      break;
    case stateShowAlarm2:  // Falls through
    case stateSetAlarm2Hr: // Falls through
    case stateSetAlarm2Min:
      showHrMin(alarm2Hr, alarm2Min);
      break;
  }
}

void alarm1Callback(void) {
  if (alarm1On) {
    alarming = true;
  }
}

void alarm2Callback(void) {
  if (alarm2On) {
    alarming = true;
  }
}

uint8_t senseLight(void) {
  return constrain(map(analogRead(A0), 0, 510, 0, 255), 1, 255);
}

event_t readButtons(void) {
  bool leftBtnNew = digitalRead(A1);
  bool rightBtnNew = digitalRead(A2);
  event_t ev = leftBtnNew || rightBtnNew ? pendingEvent : noEvent;

  if (!leftBtn && !rightBtn && (leftBtnNew || rightBtnNew)) {
    btnPressTime = millis();
    if (leftBtnNew && rightBtnNew) {
      ev = bothBtnsPress;
    } else if (leftBtnNew) {
      ev = leftBtnPress;
    } else if (rightBtnNew) {
      ev = rightBtnPress;
    }
  } else if ((leftBtn || rightBtn) && (!leftBtnNew && !rightBtnNew) && (millis() - btnPressTime > 50)) {
    if (millis() - btnPressTime <= 1000) {
      if (bothBtns) {
        ev = bothBtnsReleaseShort;
      } else if (leftBtn) {
        ev = leftBtnReleaseShort;
      } else if (rightBtn) {
        ev = rightBtnReleaseShort;
      }
    } else {
      if (bothBtns) {
        ev = bothBtnsReleaseLong;
      } else if (leftBtn) {
        ev = leftBtnReleaseLong;
      } else if (rightBtn) {
        ev = rightBtnReleaseLong;
      }
    }
    bothBtns = false;
  } else if (millis() - btnPressTime > 1000) {
    if (bothBtns) {
      ev = bothBtnsHold;
    } else if (leftBtn) {
      ev = leftBtnHold;
    } else if (rightBtn) {
      ev = rightBtnHold;
    } else {
      bothBtns = false;
    }
  }

  leftBtn = leftBtnNew;
  rightBtn = rightBtnNew;
  bothBtns = bothBtns || (leftBtn && rightBtn);

  return ev;
}

void buzzerOn(void) {
  tone(12, 440);
}

void buzzerOff(void) {
  noTone(12);
}
